import React from 'react';
import styles from './ReDashboardCard.module.css';

function ReDashboardCard() {
    return (
        <div className="d-flex justify-content-center" style={{marginTop: "6rem"}}>
            <div className="card w-25" style={{overflow: "hidden", cursor: "pointer"}}>
                <div className="card-header row">
                    <div className="col-8">
                    <small className="text-muted">Richard Hendricks</small>
                    <br />
                    <small className="text-muted">May 6, 2019</small>
                    </div>
                    <div className="col-4">
                    <img src="https://i.pravatar.cc/60?img=67" class="rounded float-right" alt="avatar" />
                    </div>
                </div>
                <div className="card-body">
                    <h4 className="card-title text-danger">Awesome trip to London</h4>
                    <p className="card-text text-justify">
                    London, the capital of England and the United Kingdom, is a 21st-century city with history stretching back to Roman times.
                    </p>
                    <a href="#" className="card-link text-danger">Read Full Post...</a>
                    <span className={styles.lineOne}></span>
                    <span className={styles.lineTwo}></span>
                </div>
            </div>
        </div>
    )
}

export default ReDashboardCard;
