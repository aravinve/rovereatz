import React from 'react';
import styles from './ReRelatedCard.module.css';


function ReRelatedCard() {
    const iconSize = ["material-icons m-2 ", styles.iconSize].join(" ");
    return (
        <div className="d-flex justify-content-center container-fluid" style={{marginTop: "6rem"}}>
            <div className="card mb-3" style={{cursor: "pointer"}}>
                <div className="row no-gutters p-2">
                    <div className="col-12 mb-2">
                            <img src="https://i.pravatar.cc/60?img=67" className="rounded float-left" alt="avatar" />
                            <span className="text-danger m-4">Richard Hendricks</span>
                            <br />
                            <small className="text-muted m-4">1.7M Followers</small>
                    </div>
                    <div className="col-12">
                        <h4 className="card-title">Awesome trip to London</h4>
                        <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                    </div>
                    <div className="col-12">
                        <div className="float-right">
                            <i className={iconSize}>favorite_border</i>
                            <i className={iconSize}>insert_comment</i>
                            <i className={iconSize}>share</i>
                        </div>
                    </div>
                </div>
                <div className="row no-gutters p-2">
                <p className="card-text text-justify">
                        London, the capital of England and the United Kingdom, is a 21st-century city with history stretching back to Roman times.
                        </p>
                        <a href="#" className="card-link text-secondary">Read Full Post...</a>
                </div>
            </div>
        </div>
    )
}

export default ReRelatedCard;
