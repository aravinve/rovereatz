import React from 'react';
import styles from './ReUserCard.module.css';

function ReUserCard() {
    const iconSize = ["material-icons m-2 ", styles.iconSize].join(" ");
    return (
            <div className="card mb-3" style={{cursor: "pointer"}}>
                <div className="row no-gutters">
                    <div className="col-md-12">
                    <img src="https://cdn.pixabay.com/photo/2014/09/11/18/23/london-441853_960_720.jpg" className="card-img" alt="Card Image" />
                    </div>
                    <div className="col-md-12">
                    <div className="card-body">
                        <h4 className="card-title">Awesome trip to London</h4>
                        <p className="card-text"><small className="text-muted">Last updated 3 mins ago</small></p>
                        <p className="card-text text-justify">
                        London, the capital of England and the United Kingdom, is a 21st-century city with history stretching back to Roman times. At its centre stand the imposing Houses of Parliament, the iconic ‘Big Ben’ clock tower and Westminster Abbey, site of British monarch coronations. Across the Thames River, the London Eye observation wheel provides panoramic views of the South Bank cultural complex, and the entire city.
                        </p>
                        <a href="#" className="card-link text-secondary">Read Full Post...</a>
                        <div className="container-fluid mt-4">
                            <span class="badge badge-pill badge-primary mr-2">Tower Bridge</span>
                            <span class="badge badge-pill badge-secondary mr-2">UK</span>
                            <span class="badge badge-pill badge-success mr-2">Photography</span>
                            <span class="badge badge-pill badge-danger mr-2">Food</span>
                            <span class="badge badge-pill badge-warning mr-2">Big Ben</span>
                            <span class="badge badge-pill badge-info mr-2">Ferry</span>
                            <span class="badge badge-pill badge-light mr-2">Thomas</span>
                            <span class="badge badge-pill badge-dark mr-2">Winter</span>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="row no-gutters p-2 mt-2" style={{borderBottom: "6px solid #F6BD60"}}>
                    <div className="col-md-8">
                        <div className="col-4">
                            <img src="https://i.pravatar.cc/60?img=67" className="rounded float-left" alt="avatar" />
                        </div>
                        <div className="col-8">
                            <span className="text-danger m-4">Richard Hendricks</span>
                            <br />
                            <small className="text-muted m-4">1.7M Followers</small>
                        </div>  
                    </div>
                    <div className="col-md-4">
                        <div className="float-right p-2 m-2">
                            <i className={iconSize}>favorite_border</i>
                            <i className={iconSize}>insert_comment</i>
                            <i className={iconSize}>share</i>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default ReUserCard;
