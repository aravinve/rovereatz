import React from 'react'

function ReListItem() {
    return (
        <React.Fragment>
            <tr>
                <td className="col-8">
                    <div contentEditable></div>
                </td>
                <td className="col-4">
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-outline-info btn-sm active">
                        <input type="radio" name="options" id="option1" autocomplete="off" checked />
                        <i className="material-icons">done</i>
                    </label>
                    <label class="btn btn-outline-info btn-sm">
                        <input type="radio" name="options" id="option2" autocomplete="off" />
                        <i className="material-icons">not_interested</i>
                    </label>
                    </div>
                </td>    
                </tr>
        </React.Fragment>
    )
}

export default ReListItem;
