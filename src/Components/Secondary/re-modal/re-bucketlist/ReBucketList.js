import React from 'react';
import ReListItem from './ReListItem';

function ReBucketList() {
    return (
        <div className="container-fluid">
            {/* <!-- Button trigger modal --> */}
            <button type="button" className="btn btn-danger" data-toggle="modal" data-target="#reBucketListModal">
            Launch BucketList Modal
            </button>
            {/* <!-- Modal --> */}
            <div className="modal fade" id="reBucketListModal" tabindex="-1" role="dialog">
            <div className="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title" id="reBucketListModalTitle">My BucketList</h4>
                        <div className="custom-control custom-switch ml-4 mt-2">
                            <input type="checkbox" className="custom-control-input" id="customSwitch2" />
                            <label className="custom-control-label" for="customSwitch2">Public/Private</label>
                        </div>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div className="modal-body">
                    <div className="row">
                        <div className="col-12 table-responsive">
                            {/* Table */}
                            <table class="table table-hover table-sm">
                                <thead>
                                    <tr>
                                    <th scope="col">Items</th>
                                    <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <ReListItem />
                                    <ReListItem />
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="mt-2">
                        <p className="text-muted">
                        Add More Rows <span className="badge badge-info mt-1 ml-1"><i className="material-icons" style={{fontSize: "16px", cursor: "pointer"}}>add</i></span>
                        </p>
                    </div>
                    <hr />
                    <div className="container">
                        <div className="text-muted float-left">
                                BucketList Stats
                            </div>
                        <div className="text-muted float-right">
                            Show/Hide
                        </div>
                    </div>
                    <br />
                    <div className="container row p-2 m-2">
                        <div className="col-4 text-center">
                            <i className="material-icons">done_all</i>
                            <p className="text-muted">Total</p>
                            <span className="badge badge-info badge-pill">16</span>
                        </div>
                        <div className="col-4 text-center">
                            <i className="material-icons">done</i>
                            <p className="text-muted">Done</p>
                            <span className="badge badge-info badge-pill">7</span>
                        </div>
                        <div className="col-4 text-center">
                            <i className="material-icons">not_interested</i>
                            <p className="text-muted">Not Done</p>
                            <span className="badge badge-info badge-pill">9</span>
                        </div>
                    </div>
                  
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" className="btn btn-outline-danger">Save List</button>
                </div>
                </div>
            </div>
            </div>
        </div>
    )
}

export default ReBucketList;
