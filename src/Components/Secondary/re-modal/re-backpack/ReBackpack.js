import React from 'react';
import backpackImg from '../../../../assets/vector/backpack.png';

function ReBackpack() {
    return (
        <div className="container-fluid">
            {/* <!-- Button trigger modal --> */}
            <button type="button" className="btn btn-danger" data-toggle="modal" data-target="#reBackpackModal">
            Launch Roverpack Modal
            </button>
            {/* <!-- Modal --> */}
            <div className="modal fade" id="reBackpackModal" tabindex="-1" role="dialog">
            <div className="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4 className="modal-title" id="reBackpackModalTitle">My Roverpack</h4>
                        <div className="custom-control custom-switch ml-4 mt-2">
                            <input type="checkbox" className="custom-control-input" id="customSwitch1" />
                            <label className="custom-control-label" for="customSwitch1">Public/Private</label>
                        </div>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div className="modal-body">
                    <div className="row">
                        <div className="col-8">
                            <div className="form-group">
                                <input class="form-control" type="text" id="travel-destination" placeholder="Enter your destination" />
                            </div>
                            <div className="form-group">
                                <input class="form-control" type="text" id="travel-items" placeholder="Enter items" />
                            </div>
                        </div>
                        <div className="col-4">
                            <a href="#" className="card-link text-secondary float-right">Unload All</a><br />
                            <a href="#" className="card-link text-secondary float-right">View All</a>
                            <div className="d-flex justify-content-center mt-4">
                                <button type="button" className="btn btn-outline-info mt-4">Add to bag</button>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-6">
                            <p className="text-secondary text-muted">Suggested Items</p>
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Tent
                                    <i className="badge-info material-icons">add</i>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-center">
                                    Passport
                                    <i className="badge-info material-icons">add</i>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-center">
                                    Knife
                                    <i className="badge-info material-icons">add</i>
                                </li>
                                <li className="list-group-item d-flex justify-content-between align-items-center">
                                    Medikit
                                    <i className="badge-info material-icons">add</i>
                                </li>
                            </ul>
                        </div>
                        <div className="col-6">
                            <img src={backpackImg} className="w-100 h-100" alt="backpack" />
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" className="btn btn-outline-danger">Load Backpack</button>
                </div>
                </div>
            </div>
            </div>
        </div>
    )
}

export default ReBackpack;
