import React from 'react';

function ReTagDialog() {
    return (
        <div className="col-6">
            {/* <!-- Button trigger modal --> */}
            <button type="button" className="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#reTagModal">
            Custom Tag
            </button>
            {/* <!-- Modal --> */}
            <div className="modal fade" id="reTagModal" tabindex="-1" role="dialog">
            <div className="modal-dialog modal-dialog-scrollable modal-md" role="document">
                <div className="modal-content">
                <div className="modal-body">
                    <div className="row">
                        <div className="col-12">
                                <div className="form-group">
                                    <label for="tag-name">Tag Name</label>
                                    <input class="form-control" type="text" id="tag-name" placeholder="#FavFood" />
                                </div>
                                <div className="form-group">
                                    <label for="tag-name">Tag Theme</label>
                                    <input class="form-control" type="text" id="tag-theme" placeholder="#Spicy" />
                                </div>
                        </div>
                    </div>
                    <div className="row no-gutters">
                        <div className="col-5 p-1">
                            <textarea className="form-control" id="tag-hint" name="taghint" placeholder="Create a new tag hint"></textarea>
                        </div>
                        <div className="col-4 p-1 border">
                            <div id="drop-div" placeholder="Drop/Generate/Upload a new tag icon"></div>
                        </div>
                        <div className="col-2 p-1">
                            <div className="col-12 mb-2">
                            <button type="button" className="btn btn-outline-info">Generate</button>
                            </div>
                            <div className="col-12 mb-2">
                            <button type="button" className="btn btn-outline-info">Upload</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" className="btn btn-outline-danger">Create Tag</button>
                </div>
                </div>
            </div>
            </div>
        </div>
    )
}

export default ReTagDialog;
