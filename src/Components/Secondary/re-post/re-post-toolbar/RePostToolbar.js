import React from 'react'

function RePostToolbar(props) {
    return (
        <div className="row border-bottom p-2">
            <div className="col-3">
                <button 
                type="button" 
                className="btn btn-outline-secondary btn-sm ml-3" 
                onClick={props.toggleDecoratePost}>
                 Decorate Post</button>
                <button 
                type="button"
                className="btn btn-outline-secondary btn-sm ml-2"
                onClick={props.toggleBudgetPost}>
                 Budget</button>
            </div>
            <div className="col-9">

            </div>
        </div>
    )
}

export default RePostToolbar;
