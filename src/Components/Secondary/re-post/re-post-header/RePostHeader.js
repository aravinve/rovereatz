import React from 'react'

function RePostHeader() {
    return (
        <div className="row border p-2">
            <div className="col-3">
                <h3 className="text-secondary ml-3">Create <span className="text-danger">New Post</span></h3>
            </div>
            <div className="col-5">
                <div class="btn-group btn-group-toggle float-left ml-4" data-toggle="buttons">
                        <label class="btn btn-outline-secondary btn-sm active">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked />
                            Travelogue
                        </label>
                        <label class="btn btn-outline-secondary btn-sm">
                            <input type="radio" name="options" id="option2" autocomplete="off" />
                            Grammalogue
                        </label>
                </div>
            </div>
            <div className="col-4 text-center">
                <button type="button" className="btn btn-outline-secondary btn-sm ml-2">
                <i class="far fa-trash-alt"></i> Discard</button>
                <button className="btn btn-outline-info btn-sm ml-2">
                <i class="fab fa-firstdraft"></i> Save Draft</button>
                <button type="button" className="btn btn-outline-danger btn-sm ml-2">
                <i class="far fa-save"></i> Create Post</button>
            </div>
        </div>
    )
}

export default RePostHeader;
