import React from 'react'
import RePostDecoratePost from './RePostDecoratePost'
import ReBudgetPost from './ReBudgetPost'

function RePostSidebar(props) {
    return (
        <div className="card">
            <div className="card-body">
                {props.isDecoratePost ? <RePostDecoratePost /> : null}
                {props.isBudgetPost ? <ReBudgetPost /> : null}
            </div>
        </div>
    )
}

export default RePostSidebar;
