import React from 'react'

function ReBudgetPost() {
    return (
        <div className="row">
            <div className="col-12 row">
                <label className="text-muted">Payment Modes Available</label>
                <div className="col-6">
                    <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="paymentMode1" />
                            <label class="form-check-label" for="paymentMode1">
                                NetBanking
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="paymentMode2" />
                            <label class="form-check-label" for="paymentMode2">
                                Credit Card
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="paymentMode3" />
                            <label class="form-check-label" for="paymentMode3">
                            Debit Card
                            </label>
                        </div>
                    </div>
                <div className="col-6">
                    <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="paymentMode4" />
                            <label class="form-check-label" for="paymentMode4">
                                GPay
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="paymentMode5" />
                            <label class="form-check-label" for="paymentMode5">
                                Cash
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="paymentMode6" />
                            <label class="form-check-label" for="paymentMode6">
                                PayTM
                            </label>
                        </div>
                    </div>
                </div>
                <div className="col-12">
                    
                </div>
        </div>
    )
}

export default ReBudgetPost;
