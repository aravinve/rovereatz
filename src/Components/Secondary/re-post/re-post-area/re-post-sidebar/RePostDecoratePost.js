import React from 'react'
import ReTagDialog from '../../../re-modal/re-tag-dialog/ReTagDialog'

function RePostDecoratePost() {
    return (
        <div className="row">
            <div class="btn-group btn-group-toggle col-12 mb-2" data-toggle="buttons">
                    <label class="btn btn-outline-info btn-sm">
                        <input type="radio" name="sidebaroptions" id="sidebaroption1" autocomplete="off" checked />
                        Photos
                    </label>
                    <label class="btn btn-outline-info btn-sm active">
                        <input type="radio" name="sidebaroptions" id="sidebaroption2" autocomplete="off" />
                        Links
                    </label>
                    <label class="btn btn-outline-info btn-sm">
                        <input type="radio" name="sidebaroptions" id="sidebaroption3" autocomplete="off" />
                        Clips
                    </label>
            </div>
            <div className="col-12 mt-2 mb-2">
                <div id="selected-content-div" style={{height: "106px"}} className="border" placeholder="Selected Content"></div>
            </div>
            <div className="col-12 mt-2 mb-2">
                <input type="text" className="form-control d-none" id="add-tag-link-input" placeholder="Add Tag Link" />
                <input type="file" className="form-control" id="upload-file-input" />
            </div>
            <div className="col-12 mt-2 mb-2">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="tagtheme-select-sidebar">Tag Theme</label>
                    </div>
                    <select class="custom-select" id="tagtheme-select-sidebar">
                        <option selected>Choose...</option>
                        <option value="1">Mountains</option>
                        <option value="2">Spicy Food</option>
                        <option value="3">Cool Climate</option>
                    </select>
                </div>
            </div>
            <div className="col-12 mt-2 mb-2">          
                    <textarea className="form-control" id="add-tag-input" name="addTagInput" placeholder="Search For Existing Tags"></textarea>
            </div>
            <div className="col-12 row mt-2 mb-2">
                <ReTagDialog />
                <div className="col-3">
                    <button className="btn btn-sm btn-outline-secondary ml-2">Cancel</button>
                </div>
                <div className="col-3">
                    <button className="btn btn-sm btn-outline-danger ml-2">Upload</button>
                </div>
              
            </div>
        </div>
    )
}

export default RePostDecoratePost;
