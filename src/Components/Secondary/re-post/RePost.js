import React, { Component } from 'react'
import RePostHeader from './re-post-header/RePostHeader'
import RePostToolbar from './re-post-toolbar/RePostToolbar'
import RePostArea from './re-post-area/RePostArea'
import RePostSidebar from './re-post-area/re-post-sidebar/RePostSidebar'
import { throwStatement } from '@babel/types'

class RePost extends Component {

    state = {
        isDecoratePost: false,
        isBudgetPost: false,
        closeSideBar: false
    }

    onToggleDecoratePost = () => {
        this.setState({isDecoratePost : true , isBudgetPost: false});
    }

    onToggleBudgetPost = () => {
        this.setState({isBudgetPost : true, isDecoratePost: false});
    }

    render() {
        const colSizeSideBar = (this.state.isDecoratePost || this.state.isBudgetPost) ? "col-3" : "d-none";
        const colSizePostArea = (this.state.isDecoratePost || this.state.isBudgetPost) ? "col-9" : "col-12";
        return (
            <div>
                <RePostHeader />
                <RePostToolbar 
                toggleDecoratePost={this.onToggleDecoratePost} 
                toggleBudgetPost={this.onToggleBudgetPost}/>
                <div className="row">
                    <div className={colSizeSideBar}>
                        <RePostSidebar
                            isDecoratePost={this.state.isDecoratePost}
                            isBudgetPost={this.state.isBudgetPost}
                        />
                    </div>
                    <div className={colSizePostArea}>
                        <RePostArea />
                    </div>
                </div>
            </div>
        )
    }
}

export default RePost;
