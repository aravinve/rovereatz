import React, { Component } from 'react'
import ReFilterTagArea from './ReFilterTagArea'
import ReFilterOptionCard from './ReFilterOptionCard'
import ReFilterRangeSlider from './ReFilterRangeSlider'
import ReIconFilter from './ReIconFilter'
import ReDateFilter from './ReDateFilter'

class ReFilter extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <ReFilterTagArea />
                </div>
                <div className="col-12">
                    <ReFilterOptionCard />
                </div>
                <div className="col-12">
                    <ReFilterOptionCard />
                </div>
                <div className="col-12">
                    <ReFilterRangeSlider />
                </div>
                <div className="col-12">
                    <ReIconFilter />
                </div>
                <div className="col-12">
                    <ReDateFilter />
                </div>
                
            </div>
        )
    }
}

export default ReFilter;
