import React, { Component } from 'react';
import styles from './ReBubbleMenu.module.css';

class ReBubbleMenu extends Component {

    toggleBubbleMenu = () => {
        document.querySelector("#noob-bubble-menu").classList.toggle("d-none");
        document.querySelector("#basic-bubble-menu").classList.toggle("d-none");
        document.querySelector("#expert-bubble-menu").classList.toggle("d-none");
        document.querySelector("#prime-bubble-menu").classList.toggle("d-none");
        document.querySelector("#bubble-money-div").classList.toggle("d-none");
        this.toggleFooter();
    }

    toggleFooter = () => {
        const footerSection = document.querySelector("#main-footer");
        const hideFooterBtn = document.querySelector("#hide-footer-btn");
        const showFooterBtn = document.querySelector("#show-footer-btn");
        footerSection.classList.add("d-none");
        hideFooterBtn.classList.add("d-none");
        showFooterBtn.classList.remove("d-none");
    }

    render() {
        return (
            <div className="d-flex justify-content-center">
                <span 
                id="noob-bubble-menu"
                className={styles.bubble + " btn btn-outline-danger animated bounceIn d-none mr-0 ml-0 mb-0 " + styles.bubbleNoob}>Noob</span>
                <span 
                id="basic-bubble-menu"
                className={styles.bubble + " btn btn-outline-danger animated bounceIn d-none mr-0 ml-0 mt-0 " + styles.bubbleBasic}>Basic</span>
                <span 
                onClick={this.toggleBubbleMenu} 
                className={styles.bubble + " btn btn-danger " + styles.bubbleRover}>Rover</span>
                <span 
                id="expert-bubble-menu"
                className={styles.bubble + " btn btn-outline-danger animated bounceIn d-none mr-0 ml-0 mb-0 " + styles.bubbleExpert}>Expert</span>
                <span 
                id="prime-bubble-menu"
                className={styles.bubble + " btn btn-outline-danger animated bounceIn d-none mr-0 ml-0 mt-0 " + styles.bubblePrime}>Prime</span>
                <div id="bubble-money-div" className={"form-group animated slideInRight d-none " + styles.bubbleInput}>
                    <label for="bubble-money-input" className="text-center">
                        Are you Stuck ? <br /> Just enter the money you have
                    </label>
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span class="input-group-text">$</span>
                        </div>
                        <input id="bubble-money-input" className="form-control" min="1" type="number" />
                    </div>
                </div>
               
            </div>
            
        )
    }
}

export default ReBubbleMenu;
