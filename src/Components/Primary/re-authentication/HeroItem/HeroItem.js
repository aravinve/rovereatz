import React from 'react';
import ShortLogo from '../../../../assets/Rovereatz_Short_Logo.png';
import styles from './HeroItem.module.css';

function HeroItem() {
    
    return (
            <div className={jumboStyle}>
            <div className="container">
                <h1 className="display-4 text-danger">
                    Let's Rovereatz
                    <img src={ShortLogo} alt="Rovereatz Short Logo" />
                </h1>
                <p className="lead">Join today & <span className="text-danger">Explore</span> with the world</p>
            </div>
        </div>
       
    )
}

const jumboStyle = ["jumbotron jumbotron-fluid mb-0 pb-0 col-12", styles.bgColor].join(" ");



export default HeroItem;
