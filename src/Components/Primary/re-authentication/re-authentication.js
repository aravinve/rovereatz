import React, { Component } from 'react'
import ReCarousel from './ReCarousel/ReCarousel';
import EntryForm from './EntryForm/EntryForm';
import HeroItem from './HeroItem/HeroItem';

class ReAuthentication extends Component {

    toggleForm() {
        const loginBody = document.querySelector("#login-body");
        const registerBody = document.querySelector("#register-body");
        const loginLink = document.querySelector("#login-link");
        const registerLink = document.querySelector("#register-link");
        loginBody.classList.toggle("d-none");
        registerBody.classList.toggle("d-none");
        loginLink.classList.toggle("active");
        registerLink.classList.toggle("active");
    }

    render() {
        return (
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <HeroItem />
                    <ReCarousel />
                </div>
                    
                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div className="row m-4 p-4">
                        <EntryForm 
                        toggleForm ={this.toggleForm} />
                    </div>
                </div>
            </div>
        )
    }
}

export default ReAuthentication;
