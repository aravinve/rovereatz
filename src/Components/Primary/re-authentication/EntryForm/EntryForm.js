import React from 'react';

function EntryForm(props) {
    return (
            <div className="card col-12">
                <div className="card-header">
                    <ul className="nav nav-tabs">
                        <li className="nav-item">
                            <a href="#" id="login-link"
                            className="text-secondary nav-link active"
                            onClick={props.toggleForm}>Login</a>
                        </li>
                        <li className="nav-item">
                            <a href="#" id="register-link"
                            className="text-secondary nav-link"
                            onClick={props.toggleForm}>Register</a>
                        </li>
                    </ul>
                </div>
                <div id="login-body" className="card-body animated fadeIn">
                    <form>
                        <div className="form-group">
                            <label for="login-email">Email</label>
                            <input type="email" name="loginEmail" className="form-control" id="login-email" placeholder="rovermaster@gmail.com" />
                        </div>
                        <div className="form-group">
                            <label for="login-password">Password</label>
                            <input type="password" name="loginPassword" className="form-control"
                            id="login-password" placeholder="secret@123" />
                            <small id="passwordHelp" class="form-text ">
                                <a href="#" className="text-muted text-secondary">Forgot Password</a>
                            </small>
                        </div>
                        <button type="submit" className="btn btn-outline-danger mb-2">Login</button>
                    </form>
                </div>
                <div id="register-body" className="card-body animated fadeIn d-none">
                    <form>
                        <div className="form-group">
                            <label for="register-email">Email</label>
                            <input type="email" name="registerEmail" className="form-control" id="register-email" placeholder="Valid Email Id" />
                        </div>
                        <div className="form-group">
                            <label for="register-password">New Password</label>
                            <input type="password" name="registerPassword" className="form-control"
                            id="register-password" placeholder="New Password" />
                        </div>
                        <div className="form-group">
                            <label for="confirm-password">Confirm Password</label>
                            <input type="password" name="confirmPassword" className="form-control"
                            id="confirm-password" placeholder="Confirm Password" />
                        </div>
                        <button type="submit" className="btn btn-outline-danger">Register</button>
                    </form>
                </div>
              
                <div className="form-group mt-4">
                        <button type="submit" className="btn btn-block btn-outline-dark animated fadeIn mr-2">
                            <i class="fab fa-google"></i>  Google
                        </button>
                        <button type="submit" className="btn btn-block btn-outline-primary animated fadeIn mr-2">
                            <i class="fab fa-facebook"></i>  Facebook
                        </button>
                    </div>
            </div>

           
    )
}

export default EntryForm;
