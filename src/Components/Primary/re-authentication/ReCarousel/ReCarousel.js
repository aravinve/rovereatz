import React from 'react'

import PicOne from '../../../../assets/carousel/pic_one.jpg';
import PicTwo from '../../../../assets/carousel/pic_two.jpg';
import PicThree from '../../../../assets/carousel/pic_three.jpg';
import PicFour from '../../../../assets/carousel/pic_four.jpg';



function ReCarousel() {

    return (
        <div className="col-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100 rounded" src={PicOne} alt="First slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 rounded" src={PicTwo} alt="Second slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 rounded" src={PicThree} alt="Third slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 rounded" src={PicFour} alt="Fourth slide" />
                    </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        </div>
    );
}

export default ReCarousel;
