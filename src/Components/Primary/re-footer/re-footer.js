import React, { Component } from 'react';
import ShortLogo from '../../../assets/Rovereatz_Short_Logo.png';
import LinkSection from './LinkSection/LinkSection';
import SocialConnectSection from './SocialConnectSection/SocialConnectSection';
import SubscribeSection from './SubscribeSection/SubscribeSection';

import styles from './re-footer.module.css';

class ReFooter extends Component {

    toggleFooter(){
        const footerSection = document.querySelector("#main-footer");
        const hideFooterBtn = document.querySelector("#hide-footer-btn");
        const showFooterBtn = document.querySelector("#show-footer-btn");
        footerSection.classList.toggle("d-none");
        hideFooterBtn.classList.toggle("d-none");
        showFooterBtn.classList.toggle("d-none");
    }

    render() {
        return (
            <div>
                <footer id="main-footer" className={footerStyle}>
                    <div className="col-lg-1 col-md-4 col-sm-12 text-center">
                        <img src={ShortLogo} className="p-2 m-2" alt="Short Logo Rovereatz" />
                    </div>
                    <LinkSection />
                    <SubscribeSection />
                    <SocialConnectSection />
                </footer>
                
                <span id="hide-footer-btn" className={hideFooterBtn} onClick={this.toggleFooter}>
                        <i className="material-icons">keyboard_arrow_down</i> Hide
                </span>
                <span id="show-footer-btn" className={showFooterBtn} onClick={this.toggleFooter}>
                        <i className="material-icons">keyboard_arrow_up</i> Show
                </span>
            </div>
        )
    }
}

const footerStyle = ["container-fullwidth bg-light row animated fadeIn", styles.footer].join(" ");

const hideFooterBtn = ["btn btn-sm btn-light animated", styles.hideFooterBtn].join(" ");
const showFooterBtn = ["btn btn-sm btn-light d-none animated", styles.showFooterBtn].join(" ");

export default ReFooter;
