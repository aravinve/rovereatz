import React from 'react'

function LinkSection() {
    return (
        <div className="col-lg-3 col-md-8 col-sm-12 row text-secondary p-4 text-lg-left text-sm-center text-xs-center" style={{userSelect:"none", cursor:"pointer"}}>
            <div className="col-6">
                <ul class="navbar-nav">
                    <li class="nav-item">About Us</li>
                    <li class="nav-item">Meet The Team</li>
                    <li class="nav-item">Carrers</li>
                </ul>
            </div>
            <div className="col-6">
                <ul class="navbar-nav">
                    <li class="nav-item">Partner with Us</li>
                    <li class="nav-item">Developer</li>
                    <li class="nav-item">Sponsers</li>
                </ul>
            </div>
        </div>
    )
}

export default LinkSection;