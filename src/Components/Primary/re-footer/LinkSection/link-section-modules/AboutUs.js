import React from "react";
import ReCarousel from "../../../re-authentication/ReCarousel/ReCarousel";

function AboutUs() {
  return (
    <div className="row mt-4">
      <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
        <ReCarousel />
      </div>

      <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
        <h1 className="text-danger">About Us</h1>
        <p className="text-center">Let's Rover without hunger</p>
      </div>
    </div>
  );
}

export default AboutUs;
