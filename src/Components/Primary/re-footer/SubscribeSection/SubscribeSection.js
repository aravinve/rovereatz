import React from 'react'

function SubscribeSection() {
    return (
        
            <div className="col-lg-6 col-md-12 col-sm-12 row p-4">
            <div className="col-3">
                <p style={{userSelect: "none"}} className="text-secondary">Subscribe to our Newsletter</p>
            </div>
            <div className="col-9">
                <input type="email" className="form-control" name="email" />
                <button className="mt-2 btn btn-outline-danger">Subscribe</button>
            </div>
        </div>        
    )
}

export default SubscribeSection;
