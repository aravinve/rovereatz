import React from 'react'

function SocialConnectSection() {
    
    return (
       <div className="col-lg-2 col-md-12 col-sm-12 p-4 text-center">
           <h4 style={{userSelect: "none"}} className="text-secondary">Let's Rover</h4>
           <span style={{cursor: "pointer", fontSize: "24px"}} className="animated fadeIn badge btn-outline-primary">
                <i class="fab fa-facebook"></i>
           </span>
           <span style={{cursor: "pointer", fontSize: "24px"}} className="animated fadeIn badge btn-outline-dark">
                <i class="fab fa-instagram"></i>
           </span>
           <span style={{cursor: "pointer", fontSize: "24px"}} className="animated fadeIn badge btn-outline-info">
                <i class="fab fa-twitter"></i>
           </span>
           <span style={{cursor: "pointer", fontSize: "24px"}} className="animated fadeIn badge btn-outline-danger">
                <i class="fab fa-google"></i>
           </span>
       </div>
    )
}

export default SocialConnectSection;