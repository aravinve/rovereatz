import React from 'react'
import ReFilter from '../../Secondary/re-filter/ReFilter'
import ReDashboardCard from '../../Secondary/re-card/re-dashboard-card/ReDashboardCard'
import ReUserCard from '../../Secondary/re-card/re-user-card/ReUserCard'

function ReListView(props) {
    const colSizeListView = (props.isFilterRoomShown) ? "col-10" : "col-12";
    return (
        <React.Fragment>
            <div className="row mt-4 mb-2 container-fluid p-2">
                <div className="col-4 text-center">
                    <button className="btn btn-outline-danger btn-block d-flex mb-2">
                        <i className="material-icons mr-2">edit</i>
                        Compose Post
                    </button>
                    <div className="list-group list-group-horizontal bg-light">
                        <button type="button" className="list-group-item list-group-item-action">
                        Home
                        </button>
                        <button type="button" className="list-group-item list-group-item-action">
                        Dashboard
                        </button>
                    </div>
                </div>
                <div className="col-6 text-center">
                        <div className="list-group list-group-horizontal bg-light mt-4">
                            <button type="button" className="list-group-item list-group-item-action">
                                Basic
                            </button>
                            <button type="button" className="list-group-item list-group-item-action">
                                Expert
                            </button>
                            <button type="button" className="list-group-item list-group-item-action">
                                Prime
                            </button>
                        </div> 
                </div>
                <div className="col-2 text-center">
                    <a className="nav-link text-muted" href="#" onClick={props.showFilterRoom}>Filters</a>
                    <a className="nav-link text-muted" href="#">Sorter</a>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-2">
                    <ReFilter />
                </div>
                <div className={colSizeListView}>
                <div className="row container-fluid no-gutters">
                        <div className="col-6">
                            <ReUserCard />
                            <ReUserCard />
                            <ReUserCard />
                            <ReUserCard />
                        </div>
                        <div className="col-6">
                            <ReUserCard />
                            <ReUserCard />
                            <ReUserCard />
                            <ReUserCard />
                        </div>
                </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default ReListView;
