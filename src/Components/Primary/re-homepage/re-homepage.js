import React, { Component } from 'react';
import ReBubbleMenu from '../../Secondary/re-bubble-menu/ReBubbleMenu';
import ReDashboardCard from '../../Secondary/re-card/re-dashboard-card/ReDashboardCard';
import ReUserCard from '../../Secondary/re-card/re-user-card/ReUserCard';
import ReRelatedCard from '../../Secondary/re-card/re-related-card/ReRelatedCard';
import ReBackpack from '../../Secondary/re-modal/re-backpack/ReBackpack';
import ReBucketList from '../../Secondary/re-modal/re-bucketlist/ReBucketList';
import ReTagDialog from '../../Secondary/re-modal/re-tag-dialog/ReTagDialog';
import RePost from '../../Secondary/re-post/RePost';
import ReListView from '../re-views/ReListView';

class ReHomepage extends Component {
  state = {
    isFilterRoomShown: false,
  };

  showFilterRoomHandler = () => {
    this.setState({ isFilterRoomShown: !this.state.isFilterRoomShown });
  };

  render() {
    return (
      <div className='container-fluid'>
        {/* <ReListView
          isFilterRoomShown={this.state.isFilterRoomShown}
          showFilterRoom={this.showFilterRoomHandler}
        /> */}
        {/* <ReBubbleMenu /> */}
        {/* <ReDashboardCard /> */}
        {/* <ReUserCard /> */}
        {/* <ReRelatedCard /> */}
        {/* <ReBackpack /> */}
        {/* <ReBucketList />
        <ReTagDialog /> */}
        <RePost />
      </div>
    );
  }
}

export default ReHomepage;
