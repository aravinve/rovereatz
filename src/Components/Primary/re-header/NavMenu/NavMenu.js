import React from 'react';

const NavMenu = () => {

    return(
        <div className="col-md-7" style={{userSelect: "none"}}>
            <form id="search-form" className="mt-2 animated fadeIn d-none">
                <input 
                    className="form-control mb-2"
                    type="search" 
                    placeholder="Enter here to search .."
                    name="search" />
            </form>
            <ul id="nav-options" className="mt-2 navbar-nav animated fadeIn bg-sm-danger" style={{fontSize: "1.1rem"}}>
                <li className="nav-item mr-2">
                    <a className="nav-link" href="#">Hot Now!</a>
                </li>
                <li className="nav-item mr-2">
                    <a className="nav-link" href="#">Top 5's</a>
                </li>
                <li className="nav-item mr-2">
                    <a className="nav-link" href="#">Categories</a>
                </li>
                <li className="nav-item mr-2">
                    <a className="nav-link" href="#">Top Rovers</a>
                </li>
                <li className="nav-item mr-2">
                    <a className="nav-link" href="#">Planner</a>
                </li>
            </ul>
        </div>
    );

}

export default NavMenu;