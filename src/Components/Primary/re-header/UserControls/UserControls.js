import React from 'react';
import styles from './UserControls.module.css';

const UserControls = (props) => {

    const iconStyle = ["material-icons", styles.iconSize].join(" ");
    const userControlStyle = [styles.usercontrols, "btn text-secondary mt-2 mb-0 ml-2 mr-2"].join(" ");
    const userControlItemsStyle = [styles.userControlItems, "dropdown-item"].join(" ");

    return(
        <div className="col-md-5 row">
            <div className="col-9 text-right">
                <span className={userControlStyle}
                    onClick={props.toggleSearch}>
                    <i className={iconStyle}>search</i>
                </span>
                <span className={userControlStyle}>
                    <i className={iconStyle}>polymer</i>
                </span>
                <span className={userControlStyle}>
                    <i className={iconStyle}>notifications</i>
                </span>
            </div>
            <div className="col-3">
                <div className="dropdown mt-2 mb-0 ml-2">
                    <button className="btn text-secondary dropdown-toggle" type="button" id="userControlDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i className={iconStyle}>account_circle</i>
                    </button>
                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userControlDropDown">
                        <p className="text-center text-muted">
                            Welcome Rover!
                        </p>
                        <div className="dropdown-divider"></div>
                        <span className={userControlItemsStyle} href="#">
                        <i className="material-icons mr-2">dashboard</i>Dashboard</span>
                        <span className={userControlItemsStyle} href="#">
                        <i className="material-icons mr-2">person</i>Personalize</span>
                        <span className={userControlItemsStyle} href="#">
                        <i className="material-icons mr-2">settings_applications</i>Settings</span>
                        <span className={userControlItemsStyle} href="#">
                        <i className="material-icons mr-2">subdirectory_arrow_right</i>Logout</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default UserControls;