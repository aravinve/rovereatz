import React, {Component}  from 'react';

import Navbar from './Navbar/Navbar';

class ReHeader extends Component{

    toggleSearch = () => {
        const searchForm = document.querySelector("#search-form");
        searchForm.classList.toggle("d-none");
        const navList = document.querySelector("#nav-options");
        navList.classList.toggle("d-none");
    }

    hideOtherSection = () => {
        const footerSection = document.querySelector("#main-footer");
        const hideFooterBtn = document.querySelector("#hide-footer-btn");
        const showFooterBtn = document.querySelector("#show-footer-btn");
        footerSection.classList.add("d-none");
        hideFooterBtn.classList.add("d-none");
        showFooterBtn.classList.remove("d-none");
    }

    render(){
        return (
                <Navbar toggleSearch={this.toggleSearch} hideOtherSection={this.hideOtherSection}/>
        )
    }

}

export default ReHeader;