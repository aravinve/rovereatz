import React from 'react';

import styles from './Logo.module.css';

import LogoImage from '../../../../assets/Rovereatz_Logo.jpg';

const Logo = () => {

    const logoStyle = [styles.logo, "navbar-brand bounce"].join(" ");
    
    return (    
            <img src={LogoImage} alt="logo" className={logoStyle}/>
            

    );
}

export default Logo;