import React from 'react';
import Logo from '../Logo/Logo';
import NavMenu from '../NavMenu/NavMenu';
import UserControls from '../UserControls/UserControls';

import styles from './Navbar.module.css';

const Navbar = (props) => {
    const navStyle = [styles.navbackground, "container-fullwidth"].join(" ");
    const navMenu = [styles.navMenu, "collapse navbar-collapse col-xl-9 col-lg-9 col-md-12 col-sm-12"].join(" ");
    return(
        <div className={navStyle}>
             <nav className="navbar navbar-expand-lg navbar-light p-0">
                <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12">
                    <button class="navbar-toggler mr-4" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" onClick={props.hideOtherSection}>
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <Logo />
                </div>
                <div id="navbarSupportedContent" className={navMenu}>
                    <NavMenu />
                    <UserControls toggleSearch={props.toggleSearch}/>
                </div>
            </nav>
        </div>
    );
}

export default Navbar;