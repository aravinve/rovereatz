import React from 'react';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'

import './App.css';

import ReHeader from '../Components/Primary/re-header/re-header';
import ReHomepage from '../Components/Primary/re-homepage/re-homepage';
import ReFooter from '../Components/Primary/re-footer/re-footer';
import ReAuthentication from '../Components/Primary/re-authentication/re-authentication';

function App() {
  
  return (
    // <Router>
    //     <React.Fragment>
    //     <Route path="/" component={<ReAuthentication />} />
    //     <Route path="/home" component={<ReHeader />} 
    //       />
    //     </React.Fragment>
    // </Router>
    <div>
      <ReHeader />
      <ReHomepage />
      <ReFooter />
    </div>
  );
  
}

export default App;
